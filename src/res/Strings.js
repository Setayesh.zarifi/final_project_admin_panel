const strings = {
  name: "Name",
  full_name: "Name Family Name",
  username: "Username",
  password: "Password",
  login_to_site: "Login to site",
  login: "Login",
  dont_have_account: "Don't have an account?",
  signup: "Sign Up",
  first_name: "Name",
  last_name: "Family Name",
  gender: "Gender",
  phone: "Phone Number",
  email: "Email",
  fill_the_form: "Fill out the required data",
  registration_successfully_done: "You are successfully registered",
  already_have_account: "Do you have an account?",
  my_forms: "My Forms",
  create_new_form: "Create a new form",
  logout: "Logout",
  companies:"Companies",
  monitoring:"Monitoring",
  addCompany_successfully_done:"New Company Added Successfully",
  addEmployee_successfully_done:"New Employee Added Successfully"
};

export default strings;
