import React, { Component } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { Menu, Layout, Icon } from "antd";
import "./AppHeader.css";
import strings from "../res/Strings";
import "../res/CssStyles.css";
import { logout } from "../util/APIUtils";
import {
  notification,
  message
} from "antd";

const { Header, Footer, Sider, Content } = Layout;

export default class AppHeader extends Component {
   constructor(props) {
    super(props);
    this.state = {
      disabledBtn: false,
      //isAuthenticated:false
    };
    this.handleLogout = this.handleLogout.bind(this);
  } 

  handleLogout(event){
    //event.preventDefault;
    logout()
          .then(async response => {
            console.log(response.status);
            await console.log("data",JSON.stringify(response));
            if(response.status==200){
                localStorage.clear();
                console.log(localStorage.getItem("ACCESS_TOKEN"));
                notification.success({
                  message: "Location App",
                  description: "successfully logged out!"
            });
            }

          })
          .catch(error => {
          
          });


  }
  render() {
    let menuItems;
    menuItems = [
      <Menu.Item key="/AddCompany">
        <Link to="/AddCompany" className="ptxt">{strings.companies}</Link>
      </Menu.Item>,
      <Menu.Item key="/Main" className="ptxt">
        <Link to="/Main">{strings.monitoring}</Link>
      </Menu.Item>
    ];
    return (
      <Header className="app-header">
        <div className="container">


        <Menu
            className=" logout_link"
            mode="horizontal"
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="/login"  onClick={this.handleLogout}>
              <Link to="/login" className="ptxt">
                {strings.logout}
              </Link>
            </Menu.Item>
          </Menu>

          <div className="app-title">
            <Link to="/">
              <Icon type="home" style={{ fontSize: "25px" ,padding:15}} />
              App Title
            </Link>
          </div>

          <Menu
            className="app-menu"
            mode="horizontal"
            style={{ lineHeight: "64px" }}
          >
            {menuItems}
          </Menu>
        </div>
      </Header>
    );
  }
}
