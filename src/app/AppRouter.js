import createHistory from "history/createBrowserHistory";
import React, { Component } from "react";
import {Redirect, Route, Switch } from "react-router-dom";
import Login from "../user/login/Login";
import Signup from "../user/signup/Signup";
import Main from "../pages/Main";
import AddEmployee from "../pages/AddEmployee";
import AddCompany from "../pages/AddCompany";

export default class AppRouter extends Component {
  render() {
    const history = createHistory();
    return (
      
        <Switch>
          <Route
            history={history}
            path="/login"
            render={props => <Login {...props} />}
          ></Route>

          <Route history={history} path="/Signup"  render={(props) => <Signup {...props} />}></Route>

          <Route history={history} path="/Main" render={(props) => <Main {...props} />}></Route>

          <Route history={history} path="/AddCompany" render={(props) => <AddCompany {...props} />}></Route>

          <Route history={history} path="/AddEmployee" render={(props) => <AddEmployee {...props} />}></Route>

          <Redirect to="/login" />
        </Switch>
    );
  }
}
