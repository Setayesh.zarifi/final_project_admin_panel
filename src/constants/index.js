export const API_BASE_URL = 'http://192.168.43.50:8000/api';

export const ACCESS_TOKEN = 'accessToken';

export const DARKER_BLUE = '#17212b';
export const DARK_BLUE = '#394a60';

export const COLORS = [
    '#cc7a00',
    '#33cc00',
    '#cc0099',
    '#2929a3',
]
