import React, { Component } from "react";
import { PageHeader, Button,Layout,Dropdown } from "antd";
import ReactDOM from 'react-dom';
import mapboxgl from 'mapbox-gl';
import AppHeader from '../../common/AppHeader'
import './MapViewStyle.css';
import 'mapbox-gl/dist/mapbox-gl.css';



mapboxgl.accessToken = 'pk.eyJ1Ijoic2V0YXllc2h6YXJpZmkiLCJhIjoiY2swNW9nY2kwM3NvdjNtbWw0Yndlbm5vdiJ9.0bJrZw6fsphRLKPHfbXWpA';

// var geojson = {
// "type": "FeatureCollection",
// "features": [
// {
// "type": "Feature",
// "properties": {
// "message": "Foo",
// "iconSize": [60, 60]
// },
// "geometry": {
// "type": "Point",
// "coordinates": [
// 52.519359,
// 29.628461
// ]
// }
// },
// {
// "type": "Feature",
// "properties": {
// "message": "Bar",
// "iconSize": [50, 50]
// },
// "geometry": {
// "type": "Point",
// "coordinates": [
// -61.2158203125,
// -15.97189158092897
// ]
// }
// },
// {
// "type": "Feature",
// "properties": {
// "message": "Baz",
// "iconSize": [40, 40]
// },
// "geometry": {
// "type": "Point",
// "coordinates": [
// -63.29223632812499,
// -18.28151823530889
// ]
// }
// }
// ]
// };


const options = [
  'one', 'two', 'three'
];
const defaultOption = options[0];


export default class MapView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: 5,
      lat: 34,
      zoom: 1.5,
      geojson :{
        
      },
      "coordinates":[],
      map:"",
      currentMarkers:[]

    };
   this.setUpMarker = this.setUpMarker.bind(this);
   
  }

  componentDidMount(){
    const { lng, lat, zoom ,width,height} = this.state;
    const mapp = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11?optimize=true',
      center: [lng, lat],
      zoom 
	  
    });
    this.setState({map:mapp})

  }

  componentWillReceiveProps(nextProps){
    this.setState({geojson:nextProps.geoJson}) ;
     console.log("props",nextProps.geoJson);
     if (this.state.currentMarkers!==null) {
        for (var i = this.state.currentMarkers.length - 1; i >= 0; i--) {
          this.state.currentMarkers[i].remove();
        }
}
     this.setUpMarker();
  }

  async setUpMarker() {
    await localStorage.getItem("ACCESS_TOKEN");
    console.log(localStorage.getItem("ACCESS_TOKEN"));
    var that = this;
    
  
	
	this.state.geojson.forEach(function(marker) {
    
        var popup = new mapboxgl.Popup({ offset: 25}).setText(
           marker.user.name + "   "  + marker.user.familyName + "   " + marker.user.phoneNum
        );
        if(marker.location !== null){
          var newMarker = new mapboxgl.Marker({transform: 'scale(0.8)'})
          .setLngLat([marker.location.longitude, marker.location.latitude])
          .setPopup(popup) 
          .addTo(that.state.map);
          that.state.currentMarkers.push(newMarker);
        }
	});

	   
	   this.state.map.on('load', () => {
			this.state.map.resize();
      });
   


    this.state.map.on('move', () => {
      const { lng, lat } = this.state.map.getCenter();
		
		this.setState({
        lng: lng.toFixed(4),
        lat: lat.toFixed(4),
        zoom: this.state.map.getZoom().toFixed(2),
      });
   });
  }




  render() {
    const { lng, lat, zoom } = this.state;

    return (
     <Layout>
      <div>
         
        
        <div className="inline-block absolute top left mt12 ml12 bg-darken75 color-white z1 py6 px12 round-full txt-s txt-bold">
        
        </div>
        <div id='map' className="absolute top right left bottom" />
      </div>
	   </Layout>
    );
}
}






