import React, { Component,Text } from "react";
import { PageHeader, Button,Layout,List ,Card ,Row,Col,Modal,Form,Input,notification} from "antd";
import ReactDOM from 'react-dom'
import './style.css';
import AppHeader from "../../common/AppHeader";
import { userCompanies , addCompany } from "../../util/APIUtils";
import CompanyItem from "../../components/CompanyItem";
import strings from "../../res/Strings";
//import { ACCESS_TOKEN } from "../../constants";
const { Header, Footer, Sider, Content } = Layout;


class AddCompany extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyName:"",
      addModalOpen: false,
      loading:false,
      "companies": [
    ]
  };
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleInputChange = this.handleInputChange.bind(this);

}

  handleInputChange(event) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: {
        value: inputValue
      }
    });
  }


 async componentDidMount() {
           if (localStorage.getItem("ACCESS_TOKEN") === null) {
            this.props.history.replace("/login")
  
          }
          let token = await localStorage.getItem("ACCESS_TOKEN");
          userCompanies(token)
          .then(async response => {
            if(response.status==200){
               this.setState({companies:response.data.companies});
            }           
          })
          .catch(error => {
            notification.error({
              message: "Location App",
              description: error.message
            });
            console.log(error.message);
          });
    
  }

   handleCancel = () => {
    this.setState({ addModalOpen: false });
  };

 
  handleSubmit() {
      console.log(this.state.companyName.value);
      if (
        this.state.companyName.value === "" 
      )
        notification.error({
          message: "Location App",
          description: strings.fill_the_form
        });
      else {
      let token =  localStorage.getItem("ACCESS_TOKEN");
      addCompany(token,this.state.companyName.value)
        .then(response => {
          console.log("response",response);
           if(response.status==200){
               notification.success({
                message: "Location App",
                description: strings.addCompany_successfully_done
          });
          userCompanies(token)
          .then(async response => {
            if(response.status==200){
               this.setState({companies:response.data.companies});
            }           
          })
          .catch(error => {
            notification.error({
              message: "Location App",
              description: error.message
            });
            console.log(error.message);
          });
            }     
         
        })
        .catch(error => {
          notification.error({
            message: "Location App",
            description:
              error.message || "Sorry! Something went wrong. Please try again!"
          });
        });
    }

  }

  render() {
    const { getFieldDecorator } = this.props.form;
     return (
 
      <Layout>
        <AppHeader />
         <Modal
          visible={this.state.addModalOpen}
          title="Add Company"
          onCancel={()=>this.setState({addModalOpen:false})}
           footer={[
            <Button key="back" onClick={this.handleCancel}>
              Cancel
            </Button>,
          ]}
        >
        <Form labelCol={{ span: 7 }} wrapperCol={{ span: 12 }} onSubmit={this.handleSubmit}>
        <Form.Item label="Company Name">
          <Input
             name="companyName"
             value={this.state.companyName.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
          <Button type="primary" onClick={()=>this.handleSubmit()} >
            Submit
          </Button>
        </Form.Item>
        </Form>
        </Modal>
        <Content >
          <List  style = {{textAlign:'center'}}
            header={
              <Row style = {{
              textAlign:'center'}}>
                <Col span={5}>
                <h2 style = {{color:'#0080ff'}}>User companies</h2>
                </Col>
                <Col span={2}>
                <Button type="primary" icon="plus" onClick={()=> this.setState({addModalOpen:true})}>
                 </Button>
                </Col>

                </Row>

            }
            bordered
            dataSource={this.state.companies}
            renderItem={item => (
        <List.Item>
            <CompanyItem company={item} navigate={()=> this.props.history.replace("/AddEmployee")}/>
        </List.Item>
      )}
    />

        </Content>
      </Layout>
    
    );
}
}

const WrappedApp = Form.create()(AddCompany);
export default WrappedApp;




