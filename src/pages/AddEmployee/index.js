import React, { Component,Text } from "react";
import { PageHeader, Button,Layout,List ,Card ,Row,Col,Modal,Form,Input,notification} from "antd";
import ReactDOM from 'react-dom'
import './style.css';
import AppHeader from "../../common/AppHeader";
import { userCompanies , companyEmployees , addEmployee} from "../../util/APIUtils";
import strings from "../../res/Strings";
import EmployeeItem from "../../components/EmployeeItem";
//import { ACCESS_TOKEN } from "../../constants";
const { Header, Footer, Sider, Content } = Layout;


class AddEmployee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:"",
      lastName:"",
      phoneNum:"",
      licensePlate:"",
      addModalOpen: false,
      loading:false,
      "employees": [

    ] 
  };
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleInputChange = this.handleInputChange.bind(this);
}

  handleInputChange(event) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: {
        value: inputValue
      }
    });
  }

  async componentDidMount() {
         let token = await localStorage.getItem("ACCESS_TOKEN");
         let companyId = await localStorage.getItem("companyId");
          companyEmployees(token,companyId)
          .then(async response => {
            if(response.status==200){
               this.setState({employees:response.data.employees});
            }           
          })
          .catch(error => {
            notification.error({
              message: "Location App",
              description: error.message
            });
            console.log(error.message);
          });
    
  }

   handleCancel = () => {
    this.setState({ addModalOpen: false });
  };

 
  handleSubmit() {
      if (
        this.state.name.value === undefined ||
        this.state.lastName.value === undefined ||
        this.state.phoneNum.value === undefined || 
        this.state.licensePlate.value === undefined
      ){
        notification.error({
          message: "Location App",
          description: strings.fill_the_form
        });
      }
      else {
        console.log(" this.state.name.value", this.state.name.value);
      const addEmployeeRequest = {
        name: this.state.name.value,
        lastName: this.state.lastName.value,
        phoneNum:this.state.phoneNum.value,
        licensePlate:this.state.licensePlate.value
      };
      let token =  localStorage.getItem("ACCESS_TOKEN");
      let companyId =  localStorage.getItem("companyId");
      addEmployee(token,companyId,addEmployeeRequest)
        .then(response => {
           if(response.status==200){
               notification.success({
                message: "Location App",
                description: strings.addEmployee_successfully_done
          });
       companyEmployees(token,companyId)
          .then(async response => {
            if(response.status==200){
               this.setState({employees:response.data.employees});
            }           
          })
          .catch(error => {
            notification.error({
              message: "Location App",
              description: error.message
            });
            console.log(error.message);
          });
            }     
         
        })
        .catch(error => {
          notification.error({
            message: "Location App",
            description:
              error.message || "Sorry! Something went wrong. Please try again!"
          });
        });
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
     return (
 
      <Layout>
        <AppHeader />
         <Modal
          visible={this.state.addModalOpen}
          title="Add Employee"
          onCancel={()=>this.setState({addModalOpen:false})}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Cancel
            </Button>
          ]}
        >
        <Form labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} onSubmit={this.handleSubmit}>
        <Form.Item label="First Name">
          <Input
             name="name"
             value={this.state.name.value}
             onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item label="Last Name">
          <Input
             name="lastName"
             value={this.state.lastName.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item label="Phone Num">
          <Input
             name="phoneNum"
             value={this.state.phoneNum.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item label="license Plate">
          <Input
             name="licensePlate"
             value={this.state.licensePlate.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
          <Button type="primary" onClick={()=>this.handleSubmit()} >
            Submit
          </Button>
        </Form.Item>
        </Form>
        </Modal>
        <Content >
          <List  style = {{textAlign:'center'}}
            header={
              <Row style = {{
              textAlign:'center'}}>
                <Col span={5}>
                <h2 style = {{color:'#0080ff'}}>List Of Employees</h2>
                </Col>
                <Col span={2}>
                <Button type="primary" icon="plus" onClick={()=> this.setState({addModalOpen:true})}>
                 </Button>
                </Col>

                </Row>

            }
            bordered
            dataSource={this.state.employees}
            /*renderItem={this.state.employees.map((item) => (
                <List.Item>
                    <EmployeeItem employee={item} user={item.userData}/>
                </List.Item>
                ))
            }*/
            renderItem={item => (
            <List.Item>
                <EmployeeItem employee={item} />
            </List.Item>
            )}
        
    />

        </Content>
      </Layout>
    
    );
}
}

const WrappedApp = Form.create()(AddEmployee);
export default WrappedApp;




