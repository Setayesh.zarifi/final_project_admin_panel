import React, { Component } from 'react'
import { PageHeader, Button,Layout,notification } from "antd";
import { userCompanies , getCompanyEmployeesLocation } from "../util/APIUtils";
import Dropdown from 'react-dropdown';
import MapView from './MapView'
import 'react-dropdown/style.css';
import AppHeader from '../common/AppHeader'


const options = [
  { value: 1, label: 'One' },
]
const defaultOption = options[0];

export default class Main extends Component {
    constructor (props) {
    super(props)
    this.state = {
 
        "companies": [
    ],
         "employees": [
       
    ],
    timerId:"",
      selected: ''
    }
    this._onSelect = this._onSelect.bind(this)
  }

 async  _onSelect (option) {
   if(this.state.timerId){
         clearInterval(this.state.timerId);
   }

    let token = await localStorage.getItem("ACCESS_TOKEN");
    this.setState({selected: option})
    localStorage.setItem("companyEmployeeIdLocation",option.value);
    this.setState({timerId:setInterval(()=>getCompanyEmployeesLocation(token , option.value)
              .then(async response => {          
                if(response.status==200){
                      this.setState({employees:response.data.employees});
                }           
              })
              .catch(error => {
                notification.error({
                  message: "Location App",
                  description: error.message
                });
                console.log(error.message);
              })
           , 2000)}) 
    

      

  }

  async componentDidMount() {
          if (localStorage.getItem("ACCESS_TOKEN") === null) {
            this.props.history.replace("/login") 
          }
          let token = await localStorage.getItem("ACCESS_TOKEN");
          var temp = []
          userCompanies(token)
          .then(async response => {
            if(response.status==200){
    
              for (var i = 0; i < response.data.companies.length; i++) {
                    const company = {
                          value: response.data.companies[i].id,
                          label: response.data.companies[i].name,
                        };
                        // this.setstate.companies.concat(company);
                        temp.push(company);
                      
                }
      
            this.setState({companies:temp})
            //console.log("employees",this.state.employees);

            }           
          })
          .catch(error => {
            notification.error({
              message: "Location App",
              description: "Token is expired!"
            });
            console.log(error.message);
          });
    
  }

  render () {
    const defaultOption = this.state.selected
    const placeHolderValue = typeof this.state.selected === 'string' ? this.state.selected : this.state.selected.label

    return (
      <Layout>
        <AppHeader />
        <Dropdown options={this.state.companies} onChange={this._onSelect} value={defaultOption} placeholder="Select an option" />
        <MapView  geoJson = {this.state.employees}/>
        </Layout>
    )
  }
}
