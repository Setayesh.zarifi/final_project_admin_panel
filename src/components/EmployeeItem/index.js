import React, { Component} from 'react'
import { PageHeader, Button,Layout,List ,Card ,Row,Col,Modal,Form,Input,notification,Icon} from "antd";
import "./style.css"
import { FaRegBuilding } from 'react-icons/fa';
import { editEmployeeDetails} from "../../util/APIUtils";
import strings from "../../res/Strings";


export default class EmployeeItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
           name:"",
           lastName:"",
           phoneNum:"",
           licensePlate:"",
           addModalOpen: false,
           loading:false,  
        };
        this.handleAction = this.handleAction.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
  }

    handleInputChange(event) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: {
        value: inputValue
      }
    });
  }

  handleSubmit(){
        if (
        this.state.name.value === undefined ||
        this.state.lastName.value === undefined ||
        this.state.phoneNum.value === undefined || 
        this.state.licensePlate.value === undefined
      ){
        notification.error({
          message: "Location App",
          description: strings.fill_the_form
        });
      }
      else {
      const addEmployeeRequest = {
        name: this.state.name.value,
        lastName: this.state.lastName.value,
        phoneNum:this.state.phoneNum.value,
        licensePlate:this.state.licensePlate.value
      };
      this.props.employee.name = this.state.name.value;
      this.props.employee.familyName = this.state.lastName.value;
      this.props.employee.phoneNum = this.state.phoneNum.value;
      this.props.employee.licensePlate = this.state.licensePlate.value;


      let token =  localStorage.getItem("ACCESS_TOKEN");
      let employeeId = localStorage.getItem("employeeId");
      let employeeUserId = localStorage.getItem("employeeUserId");
      let companyId = localStorage.getItem("companyId");
      editEmployeeDetails(token,employeeId,employeeUserId,addEmployeeRequest)
        .then(response => {
           if(response.status==200){
               notification.success({
                message: "Location App",
                description: response.data.message
          });
            }     
         
        })
        .catch(error => {
          notification.error({
            message: "Location App",
            description:
              error.message || "Sorry! Something went wrong. Please try again!"
          });
        });
    }
  }

     handleCancel = () => {
      this.setState({ addModalOpen: false });
    };

  
    handleAction() {
       localStorage.setItem("employeeId" , this.props.employee.id);
       localStorage.setItem("employeeUserId",this.props.employee.userId);
       this.setState({addModalOpen:true})
      
  }
  
    render() {
        return (
          
      <Card style = {{width:"100%",margin:"40",alignSelf:"center"}}>
        <Modal
          visible={this.state.addModalOpen}
          title="Edit Employee Information"
          onCancel={()=>this.setState({addModalOpen:false})}
           footer={[
            <Button key="back" onClick={this.handleCancel}>
              Cancel
            </Button>
          ]}
        >
        <Form labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} onSubmit={this.handleSubmit}>
        <Form.Item label="First Name">
          <Input
             name="name"
             value={this.state.name.value}
             onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item label="Last Name">
          <Input
             name="lastName"
             value={this.state.lastName.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item label="Phone Num">
          <Input
             name="phoneNum"
             value={this.state.phoneNum.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item label="license Plate">
          <Input
             name="licensePlate"
             value={this.state.licensePlate.value}
            onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
          <Button type="primary" onClick={()=>this.handleSubmit()} >
            Submit
          </Button>
        </Form.Item>
        </Form>
        </Modal>
      <Row>
      <Col span={1}><Icon type="user" style={{ fontSize: "30px" }} /></Col>
      <Col span={4} >
      <h4  style ={{ textAlign:'left'}}>{this.props.employee.name}
      </h4>
      </Col>
      <Col span={1}><Icon type="user" style={{ fontSize: "30px" }} /></Col>
      <Col span={4}>
       <h4  style ={{ textAlign:'left'}}>{this.props.employee.familyName}
      </h4></Col>
      <Col span={1}><Icon type="phone" style={{ fontSize: "30px" }} /></Col>
      <Col span={4}>
       <h4  style ={{ textAlign:'left'}}>{this.props.employee.phoneNum}
      </h4></Col>
      <Col span={1}><Icon type="car" style={{ fontSize: "30px" }} /></Col>
      <Col span={4}>
       <h4  style ={{ textAlign:'left'}}>{this.props.employee.licensePlate}
      </h4></Col>
      <Col span={4}>
        <Button type="primary" onClick={this.handleAction}>edit</Button>
      </Col>
      
    </Row>
         </Card>
        )
    }
}
