import React, { Component} from 'react'
import { Card ,Row,Col,Icon , Button,PageHeader,Layout,List ,Modal,Form,Input,notification} from "antd";
import "./style.css"
import { FaRegBuilding } from 'react-icons/fa';
import { editCompanyDetails} from "../../util/APIUtils";
import strings from "../../res/Strings";


export default class CompanyItem extends Component {
    constructor(props) {
        super(props);
        this.state = {    
           companyName:"",
           addModalOpen: false,
           loading:false,  
        };
        this.handleAction = this.handleAction.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
  }

    handleInputChange(event) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: {
        value: inputValue
      }
    });
  }

  handleSubmit(){
        if (
        this.state.companyName.value === undefined 
      ){
        notification.error({
          message: "Location App",
          description: strings.fill_the_form
        });
      }
      else {
      const editCompanyRequest = {
        name: this.state.companyName.value
      };
      this.props.company.name = this.state.companyName.value;

      let token =  localStorage.getItem("ACCESS_TOKEN");
      let companyId = this.props.company.id;
      editCompanyDetails(token,companyId,editCompanyRequest)
        .then(response => {
           if(response.status==200){
               notification.success({
                message: "Location App",
                description: response.data.message
          });
            }     
         
        })
        .catch(error => {
          notification.error({
            message: "Location App",
            description:
              error.message || "Sorry! Something went wrong. Please try again!"
          });
        });
    }
  }


  handleEdit = () =>{
     this.setState({addModalOpen:true});
  }



    handleAction() {
        localStorage.setItem('companyId',this.props.company.id);
        this.props.navigate();
        
  }

     handleCancel = () => {
      this.setState({ addModalOpen: false });
    };
  
    render() {
        return (
      <Card style = {{width:"100%",margin:"40",alignSelf:"center"}}>
             <Modal
          visible={this.state.addModalOpen}
          title="Edit Company Information"
          onCancel={()=>this.setState({addModalOpen:false})}
           footer={[
            <Button key="back" onClick={this.handleCancel}>
              Cancel
            </Button>
          ]}
        >
        <Form labelCol={{ span: 7 }} wrapperCol={{ span: 12 }} onSubmit={this.handleSubmit}>
        <Form.Item label="Company Name">
          <Input
             name="companyName"
             value={this.state.companyName.value}
             onChange={event => this.handleInputChange(event)}
           />
        </Form.Item>
         <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
          <Button type="primary" onClick={()=>this.handleSubmit()} >
            Submit
          </Button>
        </Form.Item>
        </Form>
        </Modal>
      <Row>
      <Col span={2}><FaRegBuilding size="30"/></Col>
      <Col span={8} >
      <h4  style ={{ textAlign:'left'}}>{this.props.company.name}
      </h4>
      </Col>
      <Col span={2}><Icon type="user" style={{ fontSize: "30px" }} /></Col>
      <Col span={8}>
       <h4  style ={{ textAlign:'left'}}>{this.props.company.employeeNumber}
      </h4></Col>
      <Col span={2}>
        <Button type="primary" onClick={this.handleAction}>details</Button>
      </Col>
       <Col span={2}>
        <Button type="primary" onClick={this.handleEdit}>edit</Button>
      </Col>
      
    </Row>
         </Card>
        )
    }
}
