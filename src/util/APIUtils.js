import { ACCESS_TOKEN, API_BASE_URL } from '../constants/index'

const axios = require('axios');


export function signup(signupRequest) {

    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        url: API_BASE_URL + "/register",
        data: {
            'phoneNum': signupRequest.phone,
             'password': signupRequest.password,
             'name':signupRequest.name,
             'familyName':signupRequest.familyName          
        }
    });

}

export function login(loginRequest) {

    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        url: API_BASE_URL + "/login",
        data: {
            'phoneNum': loginRequest.phone,
            'password': loginRequest.password
        }
    });

}


export function logout(){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' +  localStorage.getItem('ACCESS_TOKEN'),
        },
        url: API_BASE_URL + "/logout",
        data: {
           
        }
    });
}


export function userCompanies(token){
     return axios({
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/userCompanies",
        data: {
           
        }
    });
}

export function addCompany(token,name){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/createCompany",
        data: {
            'name': name
        }
    });
}

export function companyEmployees(token,companyId){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/getEmployee",
        data: {
            'company_id': companyId
        }
    });
}



export function addEmployee(token,companyId,addEmployeeRequest){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/addEmployee",
        data: {
            'companyId': companyId,
            'phoneNum' : addEmployeeRequest.phoneNum,
            'name' : addEmployeeRequest.name,
            'familyName' : addEmployeeRequest.lastName,
            'licensePlate' : addEmployeeRequest.licensePlate,

        }
    });
}




export function editEmployeeDetails(token,employeeId,employeeUserId,addEmployeeRequest){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/editEmployeeDetails",
        data: {
            'phoneNum' : addEmployeeRequest.phoneNum,
            'name' : addEmployeeRequest.name,
            'familyName' : addEmployeeRequest.lastName,
            'licensePlate' : addEmployeeRequest.licensePlate,
            'employeeId': employeeId,
	        'userId': employeeUserId ,

        }
    });
}



export function getCompanyEmployeesLocation(token,companyId){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/GetEmployeesLocations",
        data: {
            'company_id': companyId
        }
    });
}




export function editCompanyDetails(token,companyId,editCompanyRequest){
     return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization':'Bearer' + token,
        },
        url: API_BASE_URL + "/editCompanyDetails",
        data: {
            'company_id': companyId,
            'companyName':editCompanyRequest.name
        }
    });
}
